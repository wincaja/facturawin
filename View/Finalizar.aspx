﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Finalizar.aspx.cs" Inherits="FacturacionWeb.View.Finalizar" MasterPageFile="~/View/MasterPage.Master" %>

<%--<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Win facturación finalizar</title>

 <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



</head>--%>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>

<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">

     <div class="container">
            <p class="navbar-text navbar-right">Para finalizar <a href="#" onserverclick="btnFinalizar_Click" runat="server" class="navbar-link">Cerrar o terminar</a></p>
 


        <div class="page-header">
          <h1>Finalizando <small>proceso de facturación</small></h1>
        </div>
        <div>
             
         <div class="page-header">
        </div>
              <div class="panel-body">
              
                       <!-- List group -->
              <ul class="list-group">
                <li class="list-group-item">
                    <p>Ahora puedes descargar tus archivos PDF y XML</p>
                   
                </li>
   

                 
                <li class="list-group-item">
                     <div class="row">
                           <div class="col-xs-6 col-md-4">
                              
                               <br />
                           </div>
                          <div class="col-xs-6 col-md-4">
                          
                                    <asp:Button ID="btnDescargaPDF" Text="Descargar PDF"  runat="server"  class="btn btn-primary  btn-block" OnClick="btnDescargaPDF_Click"   />
                                    <asp:Button ID="btnDescargaXML" Text="Descargar XML"  runat="server"  class="btn btn-primary  btn-block" OnClick="btnDescargaXML_Click"  />
                              <br />
                               <asp:Label ID="lblstatus" runat="server" text="Estado: " ForeColor="Red" />

                          </div>
                          <div class="col-xs-6 col-md-4"></div>
                    </div>

                </li>
             
              </ul>
              </div>
            
            <div class="panel panel-default" >
              <!-- Default panel contents -->
      
              <div class="panel-body">
               </div>

              <!-- List group -->
              <ul class="list-group">
                <li class="list-group-item">
                   <p>También se enviarán los archivos se enviarán a:</p>

                    <h3>   <asp:Label ID="lblEmail1" runat="server" Text=""></asp:Label></h3>
                    <h5> Correo electrónico</h5>
                </li>

                <li class="list-group-item">
                     <div class="row">
                           <div class="col-xs-6 col-md-4"></div>
                          <div class="col-xs-6 col-md-4">
                          
                                    <asp:Button ID="btnFinalizar" Text="Finalizar"  runat="server"  class="btn btn-success  btn-block" OnClick="btnFinalizar_Click" />
                            
                          </div>
                          <div class="col-xs-6 col-md-4"></div>
                            
                    </div>
                 
                     
                       
                    
                   
                </li>
             
              </ul>
                <br />
                 <br />
                  <div class="panel-footer">O si prefieres puedes llamar Wincaja al  55 5782-2488 </div>
            </div>


        </div>
  
  </div>

</asp:content>

   


