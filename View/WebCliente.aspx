﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebCliente.aspx.cs" Inherits="FacturacionWeb.View.WebCliente" MasterPageFile="~/View/MasterPage.Master" %>

<%--<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Registro cliente facturación</title>

            <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"></link>
            <script src="https://code.jquery.com/jquery-1.12.4.js">

            </script><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>








    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
<link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"/>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css" />


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"/>  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> 

<link rel="stylesheet" href="/css/Header.css" type="text/css" />


     <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>







    
    <script type="text/javascript">
    function ShowMessage(message, messagetype) {
            var cssclass;
            switch (messagetype) {
                case 'Success':
                    cssclass = 'alert-success';
                break;
                case 'Error':
                    cssclass = 'alert-danger';
                break;
                case 'Warning':
                    cssclass = 'alert-warning';
                break;
                default:
                    cssclass = 'alert-info';
                }
            $('#alert_container').append('<div id="alert_div" style="margin: 0 0.5%; -webkit-box-shadow: 3px 4px 6px #999;" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' + messagetype + '!</strong> <span>' + message + '</span></div>');
}
    </script>



     <script type="text/javascript">
       function redirect() {
          location.href = 'Default.aspx';
        }
     </script>



</head>--%>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    
    <%-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>--%>
</asp:Content>

<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">



<%--    <div class="card text-center">
        <div class="card-header">
            Featured
        </div>
        <div class="card-body">
            <h5 class="card-title">Special title treatment</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
        <div class="card-footer text-muted">
            2 days ago
        </div>
    </div>--%>

    <div class="container">



        <div class="panel panel-primary">


         
             <h5 class="card-title">Ingrese los datos para su registro.</h5>

            <div class="panel-body">


                <%--<form data-toggle="validator" role="form" runat="server">--%>

                <div class="form-row">
                    <div class="col-md-6">
                        <label class="control-label" for="inputName">Clave:</label>
                        <asp:Label ID="lblCveCliente" CssClass="form-control" runat="server">Clave del cliente:</asp:Label>

                        <label class="control-label" for="txtNombre">Nombre</label>
                        <asp:TextBox runat="server" class="form-control" data-error="Porfavor ingrese su nombre" ID="txtNombre" placeholder="Nombre" type="text" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label" for="inputName">RFC</label>
                        <asp:TextBox runat="server" class="form-control" data-error="Ingrese el RFC" ID="txtRFC" placeholder="RFC" type="text" required />
                        <div class="help-block with-errors"></div>
                    </div>
                </div>



                <label class="control-label" for="txtRazonSocial">Razón Social (Debe coincidir con su constancia de situación fiscal): </label>
                <asp:TextBox runat="server" class="form-control" data-error="Ingrese la razón social" ID="txtRazonSocial" placeholder="Razón social" type="text" required />
                <div class="help-block with-errors"></div>



                <div class="form-row">
                    <div class="col-md-3">
                        <label class="control-label" for="txtTelfeno">Teléfono:</label>
                        <asp:TextBox runat="server" class="form-control" data-error="Ingrese el teléfono" ID="txtTelefono" placeholder="Teléfono" type="text" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-5">
                        <label for="txtEmail" class="control-label">Correo electrónico:</label>
                        <asp:TextBox runat="server" type="email" class="form-control" ID="txtEmail" placeholder="Email" required></asp:TextBox>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-3">
                        <label for="txtCodigopostal" class="control-label">Código postal (Debe coincidir con su constancia de situación fiscal): </label>
                        <asp:TextBox runat="server" type="numeric" MaxLength="6"  class="form-control" ID="txtCodigopostal" placeholder="Código postal" ></asp:TextBox>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>


                
                <div class="form-row">
                    <div class="col-md-6">
                        <label class="control-label" for="cboregimenfiscal">Régimen fiscal:</label>
                        <asp:DropDownList ID="DDListRegimenFiscal"   runat="server" CssClass="form-control dropdown" AutoPostBack="true" OnSelectedIndexChanged="DDListRegimenFiscal_SelectedIndexChanged">
                        </asp:DropDownList>
                         
                    </div>
                  
                </div>
                <div class="form-row">
                      <div class="col-md-6">
                         <label class="control-label" for="cboregimenfiscal">Uso CFDI:</label>
                        <asp:DropDownList ID="DDListUsoCFDI"  runat="server" CssClass="form-control form-control" AutoPostBack="false">
                        </asp:DropDownList>
                         

                    </div>
                </div>
                 <br />
                  <div class="form-row">
                      <label class="control-label" for="inputName" style="font-size:small" >* Se recomienda seleccionar un régimen fiscal y el uso del CFDI, así como agregar un CP válido para su uso en la facturación 4.0</label>
                  </div>
           
                <div class="alert alert-warning" role="alert">
                * La razón social y el código postal debe coincidir con su constancia de situación fiscal para su uso en la facturación 4.0
                </div>
                <!--<div class="form-row">
                      <div class="col-md-6"> 
                            

                              <label class="control-label" for="txtDireccion">Dirección</label>

                              <asp:TextBox runat="server" class="form-control" data-error="Ingrese la dirección" id="txtDireccion" placeholder="Dirección"  type="text"  />

                              <div class="help-block with-errors"></div>

                           
                      </div>
                      <div class="col-md-6"> 
                 

                              <label class="control-label" for="txtColonia">Colonia</label>

                              <asp:TextBox runat="server" class="form-control" data-error="Ingrese la colonia" id="txtColonia" placeholder="Colonia"  type="text"  />

                              <div class="help-block with-errors"></div>

                         
                      </div>
                 </div>-->



                <!--  <div class="form-row">
                     <div class="col-md-6">
                           

                              <label class="control-label" for="txtMunicipio">Municipio</label>

                              <asp:TextBox runat="server" class="form-control" data-error="Ingrese el municipio" id="txtMunicipio" placeholder="Municipio"  type="text"  />

                              <div class="help-block with-errors"></div>

                         
                     </div>
                     <div class="col-md-6">
                         
            

                              <label class="control-label" for="txtLocalidad">Localidad</label>

                              <asp:TextBox runat="server" class="form-control" data-error="Ingrese la localidad" id="txtLocalidad" placeholder="Localidad"  type="text"  />

                              <div class="help-block with-errors"></div>

                       
                     </div>
                 </div>-->

                <!--   <div class="form-row">
                         <div class="col-md-6">
                           

                                  <label class="control-label" for="txtCd">Ciudad</label>

                                  <asp:TextBox runat="server" class="form-control" data-error="Ingrese la ciudad" id="txtCd" placeholder="Ciudad"  type="text"  />

                                  <div class="help-block with-errors"></div>

                            
                         </div>
                         <div class="col-md-6">
                              

                                  <label class="control-label" for="txtCp">CP</label>

                                  <asp:TextBox runat="server" class="form-control" data-error="Ingrese el CP" id="txtCp" placeholder="CP"  type="text"  />

                                  <div class="help-block with-errors"></div>

                       
                         </div>

                        <div class="col-md-6">
                            

                                  <label class="control-label" for="txtPais">País</label>

                                  <asp:TextBox runat="server" class="form-control" data-error="Ingrese el país" id="txtPais" placeholder="País"  type="text"  />

                                  <div class="help-block with-errors"></div>

                            
                        </div>

                    
                    </div>-->


                <!-- <div class="form-row">
                        <div class="col-md-3">
                      

                                  <label class="control-label" for="txtTerritorio">Territorio</label>

                                  <asp:TextBox runat="server" class="form-control" data-error="Ingrese el territorio" id="txtTerritorio" placeholder="Territorio"  type="text"  />

                                  <div class="help-block with-errors"></div>

                            
                        </div>

                        <div class="col-md-3">
                       

                              <label class="control-label" for="txtNumExt">Num Ext</label>

                              <asp:TextBox runat="server" class="form-control" data-error="Ingrese el NumExt" id="txtNumExt" placeholder="NumExt"  type="text"  />

                              <div class="help-block with-errors"></div>

                      
                        </div>


                        <div class="col-md-3">
                           

                                  <label class="control-label" for="txtNumInt">Num Int</label>

                                  <asp:TextBox runat="server" class="form-control" data-error="Ingrese el NumInt" id="txtNumInt" placeholder="NumInt"  type="text"  />

                                  <div class="help-block with-errors"></div>

                            
                        </div>

                    </div>-->





                <!--     <label class="control-label" for="inputRepresentante">Representante</label>

                      <asp:TextBox runat="server" class="form-control" data-error="Ingrese el representante" id="txtRepresentante" placeholder="Representante"  type="text"  />

                      <div class="help-block with-errors"></div>

          
               

                 

             

                      <label class="control-label" for="inputObservacion">Observación</label>

                      <asp:TextBox runat="server" class="form-control" data-error="Ingrese la observación" id="txtObservacion" placeholder="Observación"  type="text"  />

                      <div class="help-block with-errors"></div>

          
                 
                 
                 
                  
              
      

                      <label class="control-label" for="inputReferencia">Referencia</label>

                      <asp:TextBox runat="server" class="form-control" data-error="Ingrese la Referencia" id="txtReferencia" placeholder="Referencia"  type="text"  />

                      <div class="help-block with-errors"></div>-->











                <br />
                <div class="col-md-6">
                     <asp:Label Text="" ID="lblError" CssClass="form-control text-red alert alert-danger" Visible="true" runat="server"> </asp:Label>
                  
                </div>
                <br />
          
          
               
              <div class="card text-right border-light mb-1">
              <div class="card-body">
  
                   <asp:Button ID="btnRegitrar" Text="Guardar" runat="server" Class="btn btn-success  " OnClick="btnRegitrar_Click" />
                     <asp:Button ID="btnIrAtras" Text="Ir atras" runat="server" Class="btn btn-primary   " OnClick="btnIrAtras_Click" UseSubmitBehavior="false" />
       
              </div>
            </div>           


             
                <div class="form-group">
                </div>

                <%--  <div class="col-md-6" >
                     <div class="form-group">
                      <asp:Button ID="btnCancelar" Text="Cancelar" runat="server" Class="btn btn-primary"  UseSubmitBehavior="false" OnClientClick="redirect()" OnClick="btnCancelar_Click"    />

                 </div>
            </div>--%>

                <div class="messagealert" id="alert_container">
                </div>



            </div>

        </div>
        <br />
        <br />


    </div>
   



</asp:content>


