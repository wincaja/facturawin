﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FacturacionWeb.View.Default" MasterPageFile="~/View/MasterPage.Master" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   


    
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="/bootstrap4-glyphicons/css/bootstrap-glyphicons.min.css" rel="stylesheet" type="text/css" />

    
    
    <meta http-equiv="refresh" content="120"/>






    <script type="text/javascript">
    function ShowMessage(message, messagetype) {
            var cssclass;
            switch (messagetype) {
                case 'Success':
                    cssclass = 'alert-success';
                break;
                case 'Error':
                    cssclass = 'alert-danger';
                break;
                case 'Warning':
                    cssclass = 'alert-warning';
                break;
                default:
                    cssclass = 'alert-info';
                }
            $('#alert_container').append('<div id="alert_div" style="margin: 0 0.5%; -webkit-box-shadow: 3px 4px 6px #999;" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' + messagetype + '!</strong> <span>' + message + '</span></div>');
        }








    </script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="container">
        <div class="signup-form-container">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group">

                            <h2 class="form-signin-heading">Ingrese los datos:</h2>
                            <label for="txtUsername">Nota de venta:</label>
                            <asp:TextBox ID="txtClaveTicket" runat="server" CssClass="form-control" placeholder="" MaxLength="10" required="" class="input-block-level" data-toggle="tooltip" data-trigger="manual" data-title="La tecla Bloq Mayús está activada" Style='text-transform: uppercase' />
                            <br />
                            <label for="txtPassword">Clave para facturar:</label>
                            <asp:TextBox ID="txtClaveFacturar" runat="server" TextMode="Password" CssClass="form-control" placeholder="" MaxLength="10" required="" Style='text-transform: uppercase' />
                            <div id="LoginModal" runat="server" visible="false" class="alert alert-danger">
                                <strong>Error!</strong>
                                <asp:Label ID="lblMessage" runat="server" />
                            </div>

                            <br />

                            <div class="form-group">
                                <asp:ScriptManager ID="ScriptManager1" runat="server" />
                                <br />
                                <asp:UpdateProgress ID="updateProgress" runat="server">
                                    <ProgressTemplate>
                                        <asp:Image ID="p_ent_aux" class="mx-auto d-block" runat="server" Height="50px" ImageAlign="AbsBottom" ImageUrl="~/images/load03.gif" Width="50px" />
                                        <%--Loading...--%>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel runat="server" ID="Panel">
                                    <ContentTemplate>
                                        <asp:Image ID="Image2" runat="server" Height="55px" ImageUrl="~/View/Captcha.aspx" Width="186px" />
                                        <h6>Enter código de verificación</h6>
                                        <asp:TextBox onDrop="blur();return false;" onpaste="return false" runat="server" class="form-control" ID="txtVerificationCode" Style='text-transform: uppercase'></asp:TextBox>
                                        <br />
                                        <br />
                                        <asp:Label runat="server" CssClass="label label-warning" ID="lblCaptchaMessage"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblError" CssClass="form-control alert alert-danger" Visible="false" runat="server"></asp:Label>
                                        <br />
                                          <asp:Button ID="btnEditarCliente" Text="Corregir mis datos" runat="server" Class="btn btn-warning btn-block btn-lg" visible ="false" OnClick="btnEditarCliente_Click"  />

                                        <asp:Button ID="btnLogin" Text="Continuar" runat="server" Class="btn btn-primary btn-block btn-lg" OnClick="btnLogin_Click" />

                                        <br />
                                        <%--<a id="btnEstatus" class="btn btn-link btn-block btn-lg" href="Estatus.aspx"  >Mis pendientes</a>--%>
                                        <asp:Button ID="btnEstadosFac" Text="Mis pendientes" runat="server" Class="btn btn-link btn-block btn-lg" OnClick="btnEstadosFac_Click" />
                                        
                                        <%--<asp:Button runat="server" id="UpdateButton" onclick="BntProgres_Click" text="Update" Class="btn btn-primary btn-block btn-lg" />--%>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <div class="messagealert" role="alert" id="alert_container">
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</asp:Content>




