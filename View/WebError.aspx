﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebError.aspx.cs" Inherits="FacturacionWeb.View.WebError" MasterPageFile="~/View/MasterPage.Master" %>

<%--<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Página no disponible.
        </div>
    </form>
</body>
</html>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>

<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">

  

         <div class="container">
            <%--<p class="navbar-text navbar-right">Para finalizar <a href="#" onserverclick="Fucfinalizar" runat="server" class="navbar-link">Cerrar o terminar</a></p>
 --%>
        <div class="page-header">
<%--          <h1> Página no disponible. <small> Winfactura</small></h1>--%>
        </div>



    
        <div>
             

            
            <div class="panel panel-default" >
              <!-- Default panel contents -->
              <div class="panel-heading"></div>
              <div class="panel-body">
                <p>Esta página actualmente no se encuentra disponible.</p>
              </div>

              <!-- List group -->
              <ul class="list-group">
                <li class="list-group-item">
                   Teléfono: 
                    
                    <h5> <span class="label label-success">55 5782-2488</span></h5>
                </li>
                <li class="list-group-item">
                     Pagina web
                    <h3>   <asp:Label ID="lblEmail2" runat="server" Text="www.wincaja.com.mx" href="www.wincaja.com.mx"></asp:Label></h3>
           
                 
                </li>

                 
                <li class="list-group-item">
                     <div class="row">
                           <div class="col-xs-6 col-md-4"></div>
                          <div class="col-xs-6 col-md-4">
                          
                                    <asp:Button ID="btnIrhome" Text="Ir al inicio"  runat="server"  class="btn btn-success  btn-block" OnClick="btnFinalizar_Click" />
                            
                          </div>
                          <div class="col-xs-6 col-md-4"></div>
                            
                    </div>
                 
                     
                       
                    
                   
                </li>
             
              </ul>
            </div>


        </div>
  
  </div>


</asp:content>
