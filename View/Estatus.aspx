﻿<%@ Page Title="" Language="C#" MasterPageFile="~/View/MasterPage.Master" AutoEventWireup="true" CodeBehind="Estatus.aspx.cs" Inherits="FacturacionWeb.View.Estatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="container">


        <div class="signup-form-container">


            <div class="form-body">



                <div class="table-responsive">
                    <asp:GridView ID="dgvListaEstatus" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-bordered bs-table"
                        DataKeyNames="documento" OnRowDeleting="dgvListaEstatus_RowDeleting"
                        >
                        <%--AllowPaging="true" OnRowDeleting="dgvListaTikets_RowDeleting"--%>
                        <HeaderStyle BackColor="#337ab7" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#ffffcc" />
                        <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                        <EmptyDataTemplate>
                            ¡No hay agregados en la lista de tickets!  
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px">
                                <ItemTemplate>
                                    <%--Botones de eliminar y editar cliente...--%>
                                    <asp:Button ID="btnDelete" runat="server" Text="Archivos" CommandName="Delete" CssClass="btn btn-danger" />
                                    <%--  <asp:Button ID="btnEdit" runat="server" Text="Editar" CssClass="btn btn-info" CommandName="Edit" />--%>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <%--Botones de grabar y cancelar la edición de registro...--%>
                                    <%--  <asp:Button ID="btnUpdate" runat="server" Text="Grabar" CssClass="btn btn-success" CommandName="Update" OnClientClick="return confirm('¿Seguro que quiere modificar los datos del ticket?');" />--%>
                                                                        <%--<asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="btn btn-default" CommandName="Cancel" />--%>
                                </EditItemTemplate>


                            </asp:TemplateField>



                            <%--campos no editables...--%>
                            <asp:BoundField DataField="documento" HeaderText="Documento " InsertVisible="False" ReadOnly="True" SortExpression="documento" ControlStyle-Width="70px" />
                            <asp:BoundField DataField="Factura" HeaderText="Factura" InsertVisible="False" ReadOnly="True" SortExpression="Factura" ControlStyle-Width="70px" />
                           <asp:BoundField DataField="EstadoTimbre" HeaderText="Estado" InsertVisible="False" ReadOnly="True" SortExpression="EstadoTimbre" ControlStyle-Width="100px" />

                            <asp:BoundField DataField="Cliente" HeaderText="Cliente" ReadOnly="True" SortExpression="Cliente" ControlStyle-Width="300px" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="FechaSolicitud" HeaderText="Fecha" ReadOnly="True" SortExpression="FechaSolicitud" ControlStyle-Width="400px" ItemStyle-HorizontalAlign="Center" />
                            <%--<asp:BoundField DataField="Cliente " HeaderText="Cliente" InsertVisible="False" ReadOnly="True" SortExpression="Cliente" ControlStyle-Width="70px" />--%>


                            <%--campos editables...--%>
                            <%-- <asp:TemplateField HeaderStyle-Width="300px" HeaderText="Contacto">
                                                                            <ItemTemplate>
                                                                                <asp:Label id="lblContactName" runat="server"><%# Eval("ContactName")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                        </Columns>

                    </asp:GridView>
                </div>
                
                <asp:Label ID="lblMessage" runat="server" Text="" Style="text-align:center"/><br /><br /><br />
              
                <h4>Reportar con: <span class="badge badge-primary"><asp:Label ID="lblrespresentable" runat="server" Text="" Style="text-align:center"/></span></h4>
                <br />
                <h4>Correo electrónico: <span class="badge badge-danger">   <asp:Label ID="lblrespresentableemail" runat="server" Text="" Style="text-align:center"/></span></h4>
                <br />

                <br />
                
                <div class="table-responsive">
                    <asp:GridView ID="dgvDetalleListaTickes" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-bordered bs-table"
                        DataKeyNames="DocumentoEx"
                        >
                        <%--AllowPaging="true" OnRowDeleting="dgvListaTikets_RowDeleting"--%>
                        <HeaderStyle BackColor="#337ab7" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#ffffcc" />
                        <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                        <EmptyDataTemplate>
                            ¡No hay agregados en la lista de tickets!  
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px">
                                <ItemTemplate>
                                    <%--Botones de eliminar y editar cliente...--%>
                                    <%--<asp:Button ID="btnDelete" runat="server" Text="Quitar" CssClass="btn btn-danger" OnClientClick="return confirm('¿Eliminar ticket?');" />--%>
                                    <%--  <asp:Button ID="btnEdit" runat="server" Text="Editar" CssClass="btn btn-info" CommandName="Edit" />--%>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <%--Botones de grabar y cancelar la edición de registro...--%>
                                    <%--  <asp:Button ID="btnUpdate" runat="server" Text="Grabar" CssClass="btn btn-success" CommandName="Update" OnClientClick="return confirm('¿Seguro que quiere modificar los datos del ticket?');" />
                                                                        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="btn btn-default" CommandName="Cancel" />--%>
                                </EditItemTemplate>


                            </asp:TemplateField>



                            <%--campos no editables...--%>
                            <asp:BoundField DataField="DocumentoEx" HeaderText="Lista notas de ventas " InsertVisible="False" ReadOnly="True" SortExpression="documento" ControlStyle-Width="200px" />
                            <%--<asp:BoundField DataField="Factura" HeaderText="Factura" InsertVisible="False" ReadOnly="True" SortExpression="Factura" ControlStyle-Width="70px" />
                            <asp:BoundField DataField="Cliente" HeaderText="Cliente" ReadOnly="True" SortExpression="Cliente" ControlStyle-Width="300px" ItemStyle-HorizontalAlign="Right" />
                            <asp:BoundField DataField="FechaSolicitud" HeaderText="Fecha" ReadOnly="True" SortExpression="FechaSolicitud" ControlStyle-Width="400px" ItemStyle-HorizontalAlign="Center" />
      --%>                      <%--<asp:BoundField DataField="Cliente " HeaderText="Cliente" InsertVisible="False" ReadOnly="True" SortExpression="Cliente" ControlStyle-Width="70px" />--%>


                            <%--campos editables...--%>
                            <%-- <asp:TemplateField HeaderStyle-Width="300px" HeaderText="Contacto">
                                                                            <ItemTemplate>
                                                                                <asp:Label id="lblContactName" runat="server"><%# Eval("ContactName")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                        </Columns>

                    </asp:GridView>
                </div>

                  

                <div class="row">
                           <div class="col-xs-6 col-md-4"></div>
                          <div class="col-xs-6 col-md-4">
                          
                                    <asp:Button ID="btnFinalizar" Text="Salir"  runat="server"  class="btn btn-success  btn-block" OnClick="btnFinalizar_Click" />
                            
                          </div>
                          <div class="col-xs-6 col-md-4"></div>
                            
                </div>

                <div class="messagealert" role="alert" id="alert_container">
                </div>

            </div>



        </div>


    </div>

  



</asp:Content>
