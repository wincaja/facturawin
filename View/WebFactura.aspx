﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFactura.aspx.cs" Inherits="FacturacionWeb.View.WebFactura" MasterPageFile="~/View/MasterPage.Master" %>

<%--<%@ OutputCache Duration="3600" VaryByParam="None" %>--%>





<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


<script>
        window.onload=function(){
        var pos=window.name || 0;
        window.scrollTo(0,pos);
        }
        window.onunload=function(){
        window.name=self.pageYOffset || (document.documentElement.scrollTop+document.body.scrollTop);
        }
        </script>





<script type="text/javascript">
    function ShowMessageFacTikect(message, messagetype) {
            var cssclass;
            switch (messagetype) {
                case 'Success':
                    cssclass = 'alert-success';
                break;
                case 'Error':
                    cssclass = 'alert-danger';
                break;
                case 'Warning':
                    cssclass = 'alert-warning';
                break;
                default:
                    cssclass = 'alert-info';
                }
             $('#alert_containerTikects').append('<div id="alert_div" style="margin: 0 0.5%; -webkit-box-shadow: 3px 4px 6px #999;" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' + messagetype + '!</strong> <span>' + message + '</span></div>');
}
    </script>
    
 <script type="text/javascript">
       function redirect() {
  location.href = 'Default.aspx';
}
 </script>
  
</asp:Content>







<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    

    




    <div class="container">
      
        <div class="container py-3">
            <h4 class="text-uppercase text-center">DATOS DE FACTURACIÓN</h4>


            <div class="card ">
                <div class="card-header bg-success text-white">
                    <h6 class="text-uppercase text-center">Buscar por rfc</h6>
                </div>






                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Datos del cliente
                                </button>
                            </h2>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">



                                <div class="card card-body">
                                    <div class="form-row">
                                        <div class="col-md-6">

                                            <div class="form-group">

                                                <%--<asp:Button ID="Button1" Text="Otro registro" runat="server" Class="btn btn-block btn-primary"   />--%>
                                                <div class="card">

                                                    <div class="card-header">
                                                        Datos RFC
                                                    </div>
                                                    <div class="card-body">
                                                        <label class="control-label" for="lbldescFactura">RFC: </label>
                                                        <div class="input-group">


                                                            <asp:TextBox runat="server" ID="txtRFC" placeholder="RFC" CssClass="form-control" autocomplete="off" MaxLength="13" required />

                                                            <span class="input-group-btn">
                                                                <asp:Button ID="btnBuscar" Text="Buscar" runat="server" Class="btn btn-block btn-primary" OnClick="btnBuscar_Click" />
                                                            </span>




                                                        </div>

                                                        <asp:Label ID="lblErrorRFC" CssClass="form-control label label-success text-success" Visible="false" runat="server"></asp:Label>
                                                        <div class="messagealert" role="alert" runat="server" id="alert_container">
                                                        </div>

                                                        <br />
                                                        <br />
                                                        <br />




                                                        <label for="DropDownListClientes">SELECCIONA TUS DATOS</label>
                                                        <asp:DropDownList ID="DropDownListClientes" runat="server" AutoPostBack="false" CssClass="form-control form-control"></asp:DropDownList>
                                                        <br />
                                                        <br />




                                                        <%--Botones de eliminar y editar cliente...--%>





                                                        <div class="row">
                                                            <div class="col">
                                                            </div>
                                                            <div class="col">
                                                                <asp:Button ID="btnBuscarDatosCliente" Text="Aceptar" runat="server" Class=" btn btn-block  btn-success" OnClick="btnBuscarDatosCliente_Click" />

                                                            </div>

                                                            <div class="col">
                                                                <asp:Button ID="btnLimpiar" Text="Limpiar" runat="server" Class="btn btn-block btn-primary" OnClick="btnLimpiar_Click" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>





                                        <div class="col-md-5 ml-auto">

                                            <div class="form-group">
                                                <div class="card">
                                                    <div class="card-header">
                                                        Otros datos 
                                                    </div>
                                                    <div class="card-body">


                                                        <label class="control-label" for="inputName">Clave:</label>
                                                        <asp:Label ID="lblClaveCliente" CssClass="form-control" runat="server">Sin registro</asp:Label>

                                                        <label class="control-label" for="lbldescNombre">Nombre: </label>
                                                        <%--  <asp:Button ID="btnEdit" runat="server" Text="Editar" CssClass="btn btn-info" CommandName="Edit" />--%>
                                                        <asp:Label ID="txtNombre" CssClass="form-control" runat="server">No registrado</asp:Label>

                                                        <label class="control-label" for="lbldescEmail">Correo electrónico: </label>
                                                        <%--Botones de grabar y cancelar la edición de registro...--%>
                                                        <asp:Label ID="txtEmail" CssClass="form-control" runat="server">No registrado</asp:Label>
                                                        <br />
                                                         <asp:Label ID="lblcp" Text ="Código postal:" runat="server"></asp:Label>
                                                         <asp:Label ID="txtCp" CssClass="form-control" runat="server">No registrado</asp:Label>
                                                        <br />
                                                        <asp:Label ID="lblregimendesc" Text="Régimen fiscal:" runat="server"></asp:Label>
                                                         <asp:Label ID="txtRegimenDesc" CssClass="form-control" runat="server">No registrado</asp:Label>
                                                        <hr />
                                                        <div class="row">
                                                            <asp:Label ID="lblCpMensaje" CssClass="form-control alert alert-danger" runat="server" Text=""></asp:Label>
                                                            <asp:Label ID="lblRegimenMensaje" CssClass="form-control alert alert-danger" runat="server" Text=""></asp:Label>
                                                        </div>

                                                     <%--   <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                          <asp:Label ID="Label1" CssClass="form-control alert alert-danger" runat="server" Text="Requiere registrar Régimen Fiscal "></asp:Label>
                                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                          </button>
                                                        </div>--%>




                                                        <div class="row">
                                                            <div class="col">
                                                                <%--  <asp:Button ID="btnUpdate" runat="server" Text="Grabar" CssClass="btn btn-success" CommandName="Update" OnClientClick="return confirm('¿Seguro que quiere modificar los datos del ticket?');" />
                                                                        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="btn btn-default" CommandName="Cancel" />--%>
                                                            </div>
                                                           
                                                            <div class="col">
                                                                <br />
                                                                <br />

                                                                <asp:Button ID="btnRegitrar" Text="Registrarme" runat="server" Class="btn btn-block btn-primary" OnClick="btnRegitrar_Click" />
                                                            </div>
                                                        </div>


                                                        <div class="messagealert" runat="server" id="alert_container1"></div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />


                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>

                    <div class="alert alert-success" runat="server" id="divMensaje1" role="alert">
                        &nbsp;<asp:Label ID="lblMensaje1" runat="server" Text=""></asp:Label>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Información</button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                Todos los datos son requeridos para llevar a cabo el proceso de facturación.
                            </div>
                        </div>
                    </div>

                </div>











            

                <div class="card-body">



                    <div class="card-header bg-danger text-white">
                        <h6 class="text-uppercase text-center">Facturación</h6>
                    </div>


                    <div class="row">
                        <div class="col-sm-8">
                         
                            <br />
                            <label class="control-label" for="">Régimen Fiscal: </label>
                            <asp:DropDownList ID="DropDownListRegimenFiscal" runat="server" CssClass="form-control form-control" AutoPostBack="false">
                            </asp:DropDownList>
                            <br />
                            <label class="control-label" for="DropDownListUsoCFDI">Uso CFDI: </label>
                            <asp:DropDownList ID="DropDownListUsoCFDI" runat="server" CssClass="form-control form-control" AutoPostBack="false">
                             
                            </asp:DropDownList>
                            <br />
                                <div class="card-header bg-white text-dark">
                                    <h6 class="text-uppercase text-center">Agregar mas notas de venta</h6>
                                </div>
                            <br />
                            <label class="control-label" for="lbldescFactura">Para agregar más Notas de venta indique: </label>
                            <br />

                            <div class="input-group">

                                <asp:TextBox runat="server" ID="txtBuscarOtroTiket" placeholder="No tikect" CssClass="form-control" autocomplete="off" MaxLength="10" />

                                <span class="input-group-btn">
                                    <asp:Button ID="btnBuscarOtroTiket" Text="Agregar" runat="server" Class="btn btn-block btn-primary" OnClick="btnBuscarOtroTiket_Click" />
                                </span>
                            </div>
                            <br />
                            <asp:TextBox runat="server" ID="txtClaveFacturarTikect" placeholder="Clave para facturar" CssClass="form-control" MaxLength="10" autocomplete="off" />
                            <br />
                            <div class="messagealert" runat="server" id="alert_containerTikects"></div>

                            <span class="badge badge-info">
                                <asp:Label ID="lblErrtikect" CssClass="form-control   bg-warning  text-white" Font-Size="XX-Small" Visible="false" runat="server"></asp:Label>
                            </span>

                            <br />

                            <br />


                            <div class="table-responsive">
                                <asp:GridView ID="dgvListaTikets" runat="server"
                                    AutoGenerateColumns="False"
                                    CssClass="table table-bordered bs-table"
                                    DataKeyNames="NoTiket"
                                    AllowPaging="true" OnRowDeleting="dgvListaTikets_RowDeleting">

                                    <HeaderStyle BackColor="#337ab7" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#ffffcc" />
                                    <EmptyDataRowStyle ForeColor="Red" CssClass="table table-bordered" />
                                    <EmptyDataTemplate>
                                        ¡No hay agregados en la lista de tickets!  
                                    </EmptyDataTemplate>

                                    <Columns>



                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <%--Botones de eliminar y editar cliente...--%>
                                                <asp:Button ID="btnDelete" runat="server" Text="Quitar" CssClass="btn btn-danger" CommandName="Delete" OnClientClick="return confirm('¿Eliminar ticket?');" />
                                                <%--  <asp:Button ID="btnEdit" runat="server" Text="Editar" CssClass="btn btn-info" CommandName="Edit" />--%>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <%--Botones de grabar y cancelar la edición de registro...--%>
                                                <%--  <asp:Button ID="btnUpdate" runat="server" Text="Grabar" CssClass="btn btn-success" CommandName="Update" OnClientClick="return confirm('¿Seguro que quiere modificar los datos del ticket?');" />
                                                                        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="btn btn-default" CommandName="Cancel" />--%>
                                            </EditItemTemplate>


                                        </asp:TemplateField>



                                        <%--campos no editables...--%>
                                        <asp:BoundField DataField="No" HeaderText="No" InsertVisible="False" ReadOnly="True" SortExpression="No" ControlStyle-Width="70px" />
                                        <asp:BoundField DataField="NoTiket" HeaderText="NoTiket" InsertVisible="False" ReadOnly="True" SortExpression="NoTiket" ControlStyle-Width="70px" />
                                        <asp:BoundField DataField="Monto" HeaderText="Monto" ReadOnly="True" SortExpression="Monto" ControlStyle-Width="300px" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="Fecha" HeaderText="Fecha" ReadOnly="True" SortExpression="Fecha" ControlStyle-Width="400px" ItemStyle-HorizontalAlign="Center" />



                                        <%--campos editables...--%>
                                        <%-- <asp:TemplateField HeaderStyle-Width="300px" HeaderText="Contacto">
                                                                            <ItemTemplate>
                                                                                <asp:Label id="lblContactName" runat="server"><%# Eval("ContactName")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                    </Columns>

                                </asp:GridView>
                            </div>

                        </div>









                        <div class="col-md-4 ml-auto">
                            <div class="card text-uppercase text-center  ">
                                <br />
                                <div class="card-header">
                                    Totales  
                                </div>
                                <div class="card-body">
                                    <%--campos no editables...--%>

                                    <br />


                                    <ul class="list-group list-group-flush">

                                        <li class="list-group-item">notas en lista:</li>
                                        <li class="list-group-item">
                                            <asp:Label ID="lbltikects" CssClass="form-control" BorderColor="White" Font-Size="Large" runat="server"></asp:Label>
                                        </li>

                                        <li class="list-group-item">Total $:</li>
                                        <li class="list-group-item">
                                            <asp:Label ID="txtTotalFac" CssClass="form-control font-weight-bold" BorderColor="White" runat="server"></asp:Label>
                                        </li>
                                    </ul>
                                    <br />

                                    <%--campos editables...--%>                                                                                          <%-- <asp:TemplateField HeaderStyle-Width="300px" HeaderText="Contacto">
                                                                            <ItemTemplate>
                                                                                <asp:Label id="lblContactName" runat="server"><%# Eval("ContactName")%></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                    <%-- <button type="button" class="btn btn-primary">
                                                                                      Tickets en lista <span class="badge badge-light">
                                                                                         
                                                                                          <span class="sr-only">unread messages</span>
                                                                                                       </span>
                                                                                    </button>--%>
                                    <%--  <div class="row align-items-end">--%>
                                    <asp:ScriptManager ID="ScriptManager1" runat="server" />
                                    <asp:UpdateProgress ID="updateProgress" runat="server">
                                        <ProgressTemplate>

                                            <%-- <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                                                                                                <span style="border-width: 0px; position: fixed; padding: 50px; background-color: #FFFFFF; font-size: 36px; left: 40%; top: 40%;">Loading ...</span>
                                                                                            </div>--%>

                                            <asp:Image ID="ImageLoad" class="mx-auto d-block" runat="server" Height="50px" ImageAlign="AbsBottom" ImageUrl="~/images/load03.gif" Width="50px" />
                                            <asp:Label ID="Label1" runat="server" CssClass="text-capitalize" Font-Size="Small" Text="Espere.."></asp:Label>
                                            <%--Loading...--%>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <br />
                                    <asp:UpdatePanel runat="server" ID="Panel">
                                        <ContentTemplate>
                                            <asp:Button ID="btnFacturar" Text="Facturar" runat="server" Class="btn btn-block btn-success" OnClick="btnFacturar_Click" />
                                            <br />
                                            <%--<span class="alert alert-danger">     </span>--%>
                                                <asp:Label ID="lblErrorFacturar"  CssClass="form-control alert alert-danger text-red"   Visible="false" runat="server"></asp:Label>
                                       
                                            <br />
                                            <br />
                                            <asp:Button ID="btnSalir" Text="Salir" runat="server" Class="btn btn-block btn-primary" OnClick="btnSalir_Click" />

                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <br />

                                </div>

                            
                            </div>
                        </div>




                        <br />


                    </div>











                    <div class="col-2">
                    </div>





                </div>

            </div>





            <%--  </div>--%>
        </div>
                  
                </div> 
              
                         
               
                    

                              
             

              

       

      
















</asp:Content>
   
       
    
    
       

        
    

                

